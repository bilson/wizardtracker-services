# wizardtracker-services

Backend services for the WizardTracker. Handles pulling data in from the
device, filtering, logging data, collecting lap times, web API requests, etc.

## Requirements
- Python 3+ (tested on 3.6)
- Redis

## License

This project is licensed under GPLv3. See `LICENSE.md` for more details.
